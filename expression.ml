(* an expresion in ocaml *)
(* Primitive Types and Values*)
2.;;
'c';;
"some string";;
true;;
23;;
32;;

1 + 2;; (* adding integer numbers*)
1. +. 2.;; (*adding floating-point numbers*)
2.14 *. (float_of_int 3);; (* using built-in function float_of_int*)

(*if expression*)
if 3 > 10 then "three greater than ten" else "three less than ten"


(*let definition
   * to bind a value to a name
   * to bind an expression to a name
   *)

let x = 10;; 
let y = 3;;

(* define x and then subtitute x to the rest of an expression*)
let x = 2 in x + 1;;

(* we got a warning of unused variable 
   * because we bind a value to x in the second
   * let expression: dont re-bind a value to an expression*)
let x  = 11 in (let x = 20 in x);;

if x > y then "x greater than y" else "y greater than x";;

(* 
   *
   * we can use annotation
   *)

let x : float = 11.12;;
let y : float = 110.11;;

if x > y then "x greater than y" else "y greater than x";;

(* 
   * Binding a name to an expression
   * here we bind and expression x + y 
   * to a name called add which take two
   * arguments(i.e, x and y)
   
   * int -> int -> int = <fun>
   * this indicate a function
   * which take two arguments of integer
   * and the last int indicates the return value of 
   * this function(i.e, integer)
   * *)
let add x y = 
  x + y 
  ;;

add 12 10;;
